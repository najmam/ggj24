extends Node2D

const INITIAL_SCREEN = "intro"
#const INITIAL_SCREEN = "investigation"
#const INITIAL_SCREEN = "spectacle"
#const INITIAL_SCREEN = "finish"

@onready var screen_scns = {
	"intro": preload("res://screens/intro/intro.tscn"),
	"investigation": preload("res://screens/investigation/investigation.tscn"),
	"spectacle": preload("res://screens/spectacle/spectacle.tscn"),
	"finish": preload("res://screens/finish/finish.tscn"),
	"credits": preload("res://screens/credits/credits.tscn"),
}

func _ready():
	Assets.load_everything()
	EventBus.req_change_screen.connect(_goto_screen)
	_goto_screen(INITIAL_SCREEN)

func _input(ev: InputEvent):
	if ev.is_action_pressed("meta_quit"):
		get_tree().quit()
	if ev.is_action_pressed("meta_fullscreen"):
		_toggle_fullscreen()
	if ev.is_action_pressed("dev_screenshot"):
		_take_screenshot()

func _goto_screen(screen_name: String):
	if $%screens.get_child_count() > 0:
		var cur_screen = $%screens.get_child(0)
		cur_screen.queue_free()
	var next_scn = screen_scns[screen_name] as PackedScene
	var screen = next_scn.instantiate()
	DialogueApi.cur_scene = screen
	$%screens.add_child(screen)

func _toggle_fullscreen():
	if DisplayServer.window_get_mode() != DisplayServer.WINDOW_MODE_FULLSCREEN:
		DisplayServer.window_set_mode(DisplayServer.WINDOW_MODE_FULLSCREEN)
	else:
		DisplayServer.window_set_mode(DisplayServer.WINDOW_MODE_WINDOWED)

func _take_screenshot():
	var capture = get_viewport().get_texture().get_image()
	var _time = Time.get_datetime_string_from_system()
	var filename = "user://screenshot_{0}.png".format({"0":_time.replace(":","-")})
	capture.save_png(filename)
	print("Screenshot saved to %s" % filename)
