extends Node2D

func _ready():
	print("[Screens] Entering Intro screen")
	$%btn_play.pressed.connect(func():
		EventBus.req_change_screen.emit("investigation"))
	$%btn_credits.pressed.connect(func():
		EventBus.req_change_screen.emit("credits"))
