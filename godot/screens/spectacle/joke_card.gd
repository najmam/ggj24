extends Control

signal picked

var _joke_spec

func init(joke_spec: Dictionary):
	_joke_spec = joke_spec

func _ready():
	$title.text = _joke_spec.title
	$title.pressed.connect(func(): picked.emit())

func is_hovered():
	return $title.is_hovered()
