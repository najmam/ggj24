extends Node2D

@export var delay_before_starting: float = 1.0
@export var delay_between_characters: float = 1.4
@export var score_animation_duration: float = 1.2
@export var nb_cards_in_hand: int = 15

var _can_end_game = true # false while debugging

var _cur_character
var _char_idx = 0
var _is_cur_dialogue_character_intro = false

var _show_cards = false
var _cur_jokes = []
var _cur_cards = []

var _cur_state # chara_blurb | pick_joke | resolve_joke

var _displayed_score = 0 # used during resolve_joke

# we keep a list so we don't tell several jokes to a single person
var _customers_served = []

const CHARS = ["sylvain", "stella", "maurice", "gaspard", "sarah"]
@onready var chara_highlights = $%character_highlights.get_children()
@onready var joke_card_scn = preload("res://screens/spectacle/joke_card.tscn")

#################### lifecycle methods

func _ready():
	print("[Screens] Entering Spectacle screen")

	_setup_cards()

	# Wait for some time then show the first character
	get_tree().create_timer(delay_before_starting).timeout.connect(func():
		_goto_character(CHARS[_char_idx]))

	DialogueManager.dialogue_ended.connect(_on_dialogue_ended)

func _input(ev: InputEvent):
	if ev.is_action_pressed("meta_1"):
		_goto_next_character()

func _process(_delta):
	# Highlight the current character
	for c in chara_highlights:
		c.visible = c.name == _cur_character

	## state: character_blurb (The character speaks)

	## state: pick_joke

	$%newcards_container.visible = _show_cards
	if _cur_state == "pick_joke":
		var fullname = Assets.character_specs[_cur_character].fullname
		$%label_pick_best_joke.text = "Choisis la meilleure blague pour %s" % fullname

	var _joke_hovered = null
	for c in _cur_cards:
		if c.is_hovered():
			_joke_hovered = c._joke_spec
	$%fulltext_container.visible = _cur_state == "pick_joke" and _joke_hovered != null
	if _joke_hovered != null:
		$%joke_fulltext.text = _joke_hovered.fulltext

	## state: resolve_joke
	$%label_score.visible = _cur_state == "resolve_joke"
	$%label_score.text = "Pécule : %d $MDR" % _displayed_score

#################### character succession

func _goto_next_character():
	_char_idx = (_char_idx + 1) % len(CHARS)
	if _char_idx == 0 and _can_end_game:
		_end_game()
	else:
		_goto_character(CHARS[_char_idx])

func _goto_character(codename: String):
	_cur_state = "chara_blurb"
	_cur_character = codename
	_is_cur_dialogue_character_intro = true
	_speak(codename, "blurb")

#################### dialogue

func _speak(codename: String, reason: String): # reason = blurb | haha | bof | fail
	# Fetch the text
	var chara = Assets.character_specs[codename]
	var blurb = chara["spectacle_%s" % reason]
	var fullname = chara.fullname

	var dialogue = DialogueManager.create_resource_from_text("~ spectacle\n%s: %s" % [fullname, blurb])
	DialogueManager.show_dialogue_balloon(dialogue, "spectacle")
	_show_cards = false
	# we could play some SFX here

func _on_dialogue_ended(_dialogue):
	_show_cards = true
	if _is_cur_dialogue_character_intro:
		_show_cards = true
		_is_cur_dialogue_character_intro = false
		_cur_state = "pick_joke"
	if _cur_state == "resolve_joke":
		await _hide_joke_feedback()
		await get_tree().create_timer(delay_between_characters).timeout
		_goto_next_character()

#################### cards

func _setup_cards():
	# Pick cards at random from the pool
	var jokes = (Assets.joke_specs as Array).duplicate()
	jokes.shuffle()
	jokes = jokes.slice(0, nb_cards_in_hand)
	_cur_jokes = jokes
	# Instantiate cards for them
	var idx = 0
	for joke in jokes:
		var card = joke_card_scn.instantiate()
		card.init(joke)
		card.picked.connect(func():
			_pick_card(idx))
		$%newcards.add_child(card)
		_cur_cards.append(card)
		idx += 1

func _pick_card(card_idx: int):
	#print("[Spectacle] pick card %d" % card_idx)
	var joke = _cur_cards[card_idx]._joke_spec
	var chara = Assets.character_specs[_cur_character]
	if chara.codename in _customers_served:
		print("%s already heard a joke" % chara.codename)
	else:
		print("[Spectacle] Telling joke '%s' to '%s'" % [joke.title, chara.fullname])
		_resolve_joke(joke, chara)
		_customers_served.append(chara.codename)

####################

func _resolve_joke(joke, chara):
	var result = JokeEvaluator.evaluate(joke, chara)
	var enjoy_str = "enjoyed" if result.enjoyed else "did not enjoy"
	print("[Spectacle] Score is %d, character %s" % [result.score_difference, enjoy_str])

	## show feedback
	_cur_state = "resolve_joke"

	GameState.score += result.score_difference
	GameState.score = max(0, GameState.score) # don't go below 0

	# animate the score going up over some period
	var tween = get_tree().create_tween()
	tween.tween_property($%label_score, "modulate", Color.WHITE, 0.2)
	tween.tween_property(self, "_displayed_score", GameState.score, score_animation_duration)
	await tween.finished
	await get_tree().create_timer(0.5).timeout
	_speak(chara.codename, "haha" if result.enjoyed else "fail")

func _hide_joke_feedback():
	var tween = get_tree().create_tween()
	tween.tween_property($%label_score, "modulate", Color.TRANSPARENT, 0.7)
	await tween.finished

####################

func _end_game():
	EventBus.req_change_screen.emit("finish")

