extends Node2D

var cur_character = null
var cur_title
var characters_seen = []
var finished = false # est-ce qu'on est passé à la scène Spectacle

#@onready var _character_polygons = $%character_polygons.get_children()
@onready var _character_sprite_overlays = $%character_sprite_overlays.get_children()
var _hovered_character = null

############################### lifecycle methods

func _ready():
	print("[Screens] Entering Investigation screen")

	# Lorsqu'on appuie sur "Passer sur scène",
	# déclencher le dialogue avec le barman.
	$%btn_next_screen.pressed.connect(func():
		_open_dialogue("barman", "on_pressed_goto_spectacle"))

	# Lorsqu'un dialogue vient de se terminer, désactive le perso en question.
	DialogueManager.dialogue_ended.connect(func(_dialogue):
		if finished: # ne pas écouter les signaux déclenchés après qu'on ait quitté cette scène
			return
		if cur_character not in characters_seen:
			characters_seen.append(cur_character)
		cur_character = null
		if _has_seen_all_characters() and not finished:
			finished = true
			_open_dialogue("barman", "on_seen_all_characters")
	)
	_setup_characters()
	_open_dialogue("barman", "on_scene_start")

func _input(ev: InputEvent):
	var inside_dialogue = cur_character != null
	if ev.is_action_pressed("game_pick_character"):
		if _hovered_character != null and not inside_dialogue:
			_open_dialogue(_hovered_character)

func _process(_dt):
	_hovered_character = _find_hovered_character()
	for poly in _character_sprite_overlays:
		poly.visible = poly.name == _hovered_character

###############################

func _has_seen_all_characters():
	return Assets.get_character_codenames().all(func(c: String): return c in characters_seen)

func _setup_characters():
	for chara_codename in Assets.get_character_codenames():
		#var chara = Assets.character_specs[chara_codename]
		## bind codenames to areas
		$%character_areas.get_node(chara_codename).set_meta("character_codename", chara_codename)

func _open_dialogue(chara_codename: String, title: String = "investigation"):
	print("[Dialogue] Open dialogue for %s" % chara_codename)
	cur_character = chara_codename
	cur_title = title
	var dialogue = Assets.get_dialogue(chara_codename)
	DialogueManager.show_dialogue_balloon(dialogue, title, [DialogueApi])

func _find_hovered_character():
	var mp = get_global_mouse_position()
	var query = PhysicsPointQueryParameters2D.new()
	query.collide_with_areas = true
	query.collision_mask = 8 # character_areas
	query.collide_with_bodies = false
	query.position = mp
	var space_2d = get_world_2d().direct_space_state
	for response in space_2d.intersect_point(query):
		var area = response.collider as Area2D
		return area.get_meta("character_codename")
	return null

## Exposed to DialogueApi

func goto_spectacle():
	EventBus.req_change_screen.emit("spectacle")
