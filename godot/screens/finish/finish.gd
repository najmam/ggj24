extends Node2D

func _ready():
	print("[Screens] Entering Finish screen")
	print("Final score : %d" % GameState.score)
	$%score.text = "Pécule de la soirée : %d $MDR" % GameState.score
	_open_dialogue("barman", "finish")

func _open_dialogue(chara_codename: String, title: String = "investigation"):
	print("[Dialogue] Open dialogue for %s" % chara_codename)
	var dialogue = Assets.get_dialogue(chara_codename)
	DialogueManager.show_dialogue_balloon(dialogue, title, [DialogueApi])

## Exposed to DialogueApi

func restart_game():
	print("[Finish] Will go to Intro screen and reset game state")
	EventBus.req_change_screen.emit("intro")
	GameState.reset()
