extends Node

func evaluate(_joke: Dictionary, _chara: Dictionary) -> Dictionary:
	var score_diff = 5
	for jattr in _joke.attrs:
		if jattr in _chara.enjoys:
			score_diff += 10
		if jattr in _chara.hates:
			score_diff -= 5

	score_diff = max(0,score_diff)

	var enjoyed = score_diff >= 5

	return {
		"enjoyed": enjoyed,
		"score_difference": score_diff,
	}
