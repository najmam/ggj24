extends Node

var cur_scene: Node

func goto_spectacle():
	if cur_scene.has_method("goto_spectacle"):
		cur_scene.goto_spectacle()

func restart_game():
	if cur_scene.has_method("restart_game"):
		cur_scene.restart_game()
