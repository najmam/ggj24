extends Node

signal req_change_screen(dest_screen: String) # intro | investigation | spectacle | finish
signal recorded_chara_info()
