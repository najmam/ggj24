extends Node

#["titre", "contenu", "attribut1", "attribut2", "attribut3", "attribut4", "attribut5", "attribut6"]
#["codename", "name", "spectacle_blurb", "spectacle_haha", "spectacle_bof", "spectacle_fail", "enjoys1", "enjoys2", "enjoys3", "hates1", "hates2", "hates3"]

func load_everything():
	var sheets = JSON.parse_string(FileAccess.get_file_as_string("res://assets-generated/sheets.json"))
	var idx = 0
	for joke in sheets.jokes:
		var j = {}
		j.uid = idx
		idx += 1
		j.title = joke.titre
		j.fulltext = joke.contenu
		j.fulltext = j.fulltext.replace("[SEP]", "\n\n")
		j.fulltext = j.fulltext.replace("[n]", "\n")
		j.attrs = []
		for k in joke.keys():
			if k.begins_with("attribut"):
				var v = joke[k]
				if v != "":
					j.attrs.append(v)
		joke_specs.append(j)

	for chara in sheets.characters:
		var c = {}
		c.codename = chara.codename
		c.fullname = chara.name
		c.enjoys = []
		c.hates = []
		for k in ["spectacle_blurb", "spectacle_haha", "spectacle_bof", "spectacle_fail"]:
			c[k] = chara[k]
		for k in chara.keys():
			var v = chara[k]
			if v == "":
				continue
			if k.begins_with("enjoys"):
				c.enjoys.append(v)
			if k.begins_with("hates"):
				c.hates.append(v)
		#var path_prefix = "res://assets/characters/%s/" % chara.codename
		#c.img_investigation_inworld = load(path_prefix + "investigation.png")
		#c.img_portrait = load(path_prefix + "portrait.png")
		#c.img_spectacle_fullsize = load(path_prefix + "spectacle.png")
		character_specs[chara.codename] = c

	# load dialogues
	for chara_codename in character_specs:
		var dialogue = load("res://assets/dialogue/%s.dialogue" % chara_codename)
		character_specs[chara_codename]["dialogue"] = dialogue

var character_specs = {
	#"alice": {
		#"codename": "sarah",
		#"fullname":"Sarah",
		#"spectacle_blurb": "",
		#"spectacle_haha": "",
		#"spectacle_bof": "",
		#"spectacle_fail": "",
		#"enjoys": ["nourriture", "intello", "jeu de mot"],
		#"hates": ["cul", "absurde"],
		#"img_investigation_inworld": null, # Sprite2D
		#"img_portrait": null, # Sprite2D
		#"img_spectacle_fullsize": null, # Sprite2D
	#},
}

var joke_specs = []
var joke_attributes = []

func get_dialogue(chara: String) -> DialogueResource:
	return character_specs[chara]["dialogue"] as DialogueResource

## Everyone except the barman
func get_character_codenames() -> Array[String]:
	var res = [] as Array[String]
	for c in character_specs:
		if c != "barman":
			res.append(c)
	return res
