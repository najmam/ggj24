extends Node

func _ready():
	EventBus.req_change_screen.connect(_on_change_screen)
	$bgm_investigation.play()

func _on_change_screen(screen_name: String):
	print("[AudioManager] on change screen %s" % screen_name)
	if screen_name == "intro":
		$bgm_rumble.stop()
		$bgm_spectacle.stop()
		($bgm_investigation.stream as AudioStreamOggVorbis).loop = true
		$bgm_investigation.play()
	if screen_name == "spectacle":
		$bgm_investigation.stop()
		$bgm_rumble.play()
		($bgm_rumble as AudioStreamPlayer).finished.connect(func():
			($bgm_spectacle.stream as AudioStreamOggVorbis).loop = true
			$bgm_spectacle.play())
