#!/bin/bash
set -e
set -o pipefail

curl --silent --show-error --output godot/assets-generated/sheets.json "http://misc.njmm.fr/ggj24.php?sheets=jokes,characters"
